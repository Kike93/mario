{
    "id": "f2a21416-4c37-4b72-9594-697d4b992fe0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spt_wall",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c36ed6e1-6639-42ae-a3c2-464538cf902b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f2a21416-4c37-4b72-9594-697d4b992fe0",
            "compositeImage": {
                "id": "dbd0a7ff-a688-4e5a-9d9b-bebe26d30087",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c36ed6e1-6639-42ae-a3c2-464538cf902b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "078996c8-e1fe-4f73-b56a-4dacb3ffaaf9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c36ed6e1-6639-42ae-a3c2-464538cf902b",
                    "LayerId": "8bf9e7f4-7ce0-48de-bda1-1b133bad247e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "8bf9e7f4-7ce0-48de-bda1-1b133bad247e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f2a21416-4c37-4b72-9594-697d4b992fe0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": true,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}