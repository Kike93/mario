{
    "id": "b71a3d43-6d20-4a3b-ba03-fd3fe51d23d3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spt_tuberiaLeftEnemy",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 27,
    "bbox_left": 3,
    "bbox_right": 54,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "24fa8162-dad7-4a07-8314-81a7bf303c22",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b71a3d43-6d20-4a3b-ba03-fd3fe51d23d3",
            "compositeImage": {
                "id": "fa15ee6c-cb77-4d0f-96e8-cf0763c0b227",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "24fa8162-dad7-4a07-8314-81a7bf303c22",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e8b0950a-7b9a-4865-8ab8-69a5f4f47dad",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "24fa8162-dad7-4a07-8314-81a7bf303c22",
                    "LayerId": "ce05d535-d6fc-468d-bdd1-0ff625870f42"
                }
            ]
        },
        {
            "id": "83f2457c-0f4e-4049-814e-4f26aa033e20",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b71a3d43-6d20-4a3b-ba03-fd3fe51d23d3",
            "compositeImage": {
                "id": "68244711-a351-4d9d-ae30-3a2b4a5da477",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "83f2457c-0f4e-4049-814e-4f26aa033e20",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ffd01c79-8f21-4f2e-994c-f095e5ae08e1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "83f2457c-0f4e-4049-814e-4f26aa033e20",
                    "LayerId": "ce05d535-d6fc-468d-bdd1-0ff625870f42"
                }
            ]
        },
        {
            "id": "0d7800cf-138a-47e4-8719-87a8939bfa6e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b71a3d43-6d20-4a3b-ba03-fd3fe51d23d3",
            "compositeImage": {
                "id": "f96525db-a5d3-4e36-b6f0-9b598cbb5945",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0d7800cf-138a-47e4-8719-87a8939bfa6e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d35e4a48-2ae1-4f5f-a7ed-c658cee6e293",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0d7800cf-138a-47e4-8719-87a8939bfa6e",
                    "LayerId": "ce05d535-d6fc-468d-bdd1-0ff625870f42"
                }
            ]
        },
        {
            "id": "df8aaaaa-9934-4ee9-a693-851934b733ec",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b71a3d43-6d20-4a3b-ba03-fd3fe51d23d3",
            "compositeImage": {
                "id": "2288635c-02f3-4a1e-b0c1-6b95afc93272",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "df8aaaaa-9934-4ee9-a693-851934b733ec",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "977a72d2-79d8-4489-bd12-265fda6b6fbe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "df8aaaaa-9934-4ee9-a693-851934b733ec",
                    "LayerId": "ce05d535-d6fc-468d-bdd1-0ff625870f42"
                }
            ]
        },
        {
            "id": "e6a933d2-3caa-420b-a817-eb75bac54c8b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b71a3d43-6d20-4a3b-ba03-fd3fe51d23d3",
            "compositeImage": {
                "id": "a44db0e7-18bb-46dc-9b17-fa43b1325af9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e6a933d2-3caa-420b-a817-eb75bac54c8b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d1daa39c-9178-4ee5-9fa9-e567f49022fe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e6a933d2-3caa-420b-a817-eb75bac54c8b",
                    "LayerId": "ce05d535-d6fc-468d-bdd1-0ff625870f42"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 28,
    "layers": [
        {
            "id": "ce05d535-d6fc-468d-bdd1-0ff625870f42",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b71a3d43-6d20-4a3b-ba03-fd3fe51d23d3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 55,
    "xorig": 27,
    "yorig": 14
}