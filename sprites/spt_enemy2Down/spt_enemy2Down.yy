{
    "id": "335a9108-11d9-4d5b-ad15-65f4fa063869",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spt_enemy2Down",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 12,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6e36f2f5-75d2-4cf2-96c8-5871c4332e6f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "335a9108-11d9-4d5b-ad15-65f4fa063869",
            "compositeImage": {
                "id": "6860b336-e35a-44a7-b45c-9c713b9e24b4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6e36f2f5-75d2-4cf2-96c8-5871c4332e6f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "75ad4c19-f250-4202-9476-2218a6e8b1fa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6e36f2f5-75d2-4cf2-96c8-5871c4332e6f",
                    "LayerId": "bae22863-18af-42f8-a08b-a14e4948bb00"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 13,
    "layers": [
        {
            "id": "bae22863-18af-42f8-a08b-a14e4948bb00",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "335a9108-11d9-4d5b-ad15-65f4fa063869",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 6
}