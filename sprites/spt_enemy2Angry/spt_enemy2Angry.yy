{
    "id": "2b57c791-9186-4f89-8081-188056ee67dc",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spt_enemy2Angry",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 14,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "88ce2b8b-41ce-4dec-8f40-baca66d0a122",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2b57c791-9186-4f89-8081-188056ee67dc",
            "compositeImage": {
                "id": "67ba333b-966d-4e1e-afd4-603b98c1e944",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "88ce2b8b-41ce-4dec-8f40-baca66d0a122",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cbcd6999-abaa-4094-950f-b8a2fc37891f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "88ce2b8b-41ce-4dec-8f40-baca66d0a122",
                    "LayerId": "200d3621-be6d-4140-adf6-8de535107eaa"
                }
            ]
        },
        {
            "id": "4ace5db7-e735-43af-89b5-ab04ab415db7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2b57c791-9186-4f89-8081-188056ee67dc",
            "compositeImage": {
                "id": "dd11f721-4897-48d3-844d-21367336a37a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4ace5db7-e735-43af-89b5-ab04ab415db7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ae4f4f02-ea61-4c2a-80dd-92cd02b6eb44",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4ace5db7-e735-43af-89b5-ab04ab415db7",
                    "LayerId": "200d3621-be6d-4140-adf6-8de535107eaa"
                }
            ]
        },
        {
            "id": "eba0381f-06b3-4e5e-8782-f1ff8043d19f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2b57c791-9186-4f89-8081-188056ee67dc",
            "compositeImage": {
                "id": "ad403603-1c7f-45fc-a9a7-a5e29bcb2671",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eba0381f-06b3-4e5e-8782-f1ff8043d19f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dfca2727-0f93-4a6e-9be5-b3283a674166",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eba0381f-06b3-4e5e-8782-f1ff8043d19f",
                    "LayerId": "200d3621-be6d-4140-adf6-8de535107eaa"
                }
            ]
        },
        {
            "id": "288d4e3e-9be2-4999-8e02-52cac8e46509",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2b57c791-9186-4f89-8081-188056ee67dc",
            "compositeImage": {
                "id": "4b938979-647d-4d04-bf13-1f7816523c6e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "288d4e3e-9be2-4999-8e02-52cac8e46509",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1662be77-b048-4da6-a23b-d9fdb7a5a78c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "288d4e3e-9be2-4999-8e02-52cac8e46509",
                    "LayerId": "200d3621-be6d-4140-adf6-8de535107eaa"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 15,
    "layers": [
        {
            "id": "200d3621-be6d-4140-adf6-8de535107eaa",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2b57c791-9186-4f89-8081-188056ee67dc",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 11,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 7
}