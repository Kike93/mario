{
    "id": "c4963d42-e675-4704-b94c-fda4412e68c0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spt_tubRightEnemy2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 0,
    "bbox_right": 43,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f22476b4-2534-426d-8fa4-d1460c505677",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c4963d42-e675-4704-b94c-fda4412e68c0",
            "compositeImage": {
                "id": "e0dadecb-235d-4bf3-a90d-cb63080cf0d2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f22476b4-2534-426d-8fa4-d1460c505677",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4fd9ef5e-54a7-4a00-860b-781b0d7235ba",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f22476b4-2534-426d-8fa4-d1460c505677",
                    "LayerId": "0109caad-f954-4b24-94fc-0ee74c447707"
                }
            ]
        },
        {
            "id": "cb42b59d-4caf-4886-864e-38e3a9b8eacf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c4963d42-e675-4704-b94c-fda4412e68c0",
            "compositeImage": {
                "id": "da6dbe32-a198-49ac-bfee-7eb77b733267",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cb42b59d-4caf-4886-864e-38e3a9b8eacf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "361d35b5-ea1f-4a0b-8858-9530ca0b80f0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cb42b59d-4caf-4886-864e-38e3a9b8eacf",
                    "LayerId": "0109caad-f954-4b24-94fc-0ee74c447707"
                }
            ]
        },
        {
            "id": "3551bb17-4d11-4f7d-88da-a8582905eeed",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c4963d42-e675-4704-b94c-fda4412e68c0",
            "compositeImage": {
                "id": "4ae099e3-ad15-4fc6-970e-1b517f93f995",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3551bb17-4d11-4f7d-88da-a8582905eeed",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9d73c0ad-0079-4026-8f63-1aeef4ab8cd9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3551bb17-4d11-4f7d-88da-a8582905eeed",
                    "LayerId": "0109caad-f954-4b24-94fc-0ee74c447707"
                }
            ]
        },
        {
            "id": "43a5df7b-2a85-4367-9603-174986b8c672",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c4963d42-e675-4704-b94c-fda4412e68c0",
            "compositeImage": {
                "id": "a1b7503f-e9d2-4a58-b638-c13344ec4ae6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "43a5df7b-2a85-4367-9603-174986b8c672",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "85bfacb5-d3bf-44ad-ac55-6b2ea523290f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "43a5df7b-2a85-4367-9603-174986b8c672",
                    "LayerId": "0109caad-f954-4b24-94fc-0ee74c447707"
                }
            ]
        },
        {
            "id": "c8c3de34-3aa0-4477-a489-1e9f85425120",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c4963d42-e675-4704-b94c-fda4412e68c0",
            "compositeImage": {
                "id": "96afd3d6-b446-4bf0-b00c-027ee4f91911",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c8c3de34-3aa0-4477-a489-1e9f85425120",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2cfd19fb-e63f-4b64-bf0e-e16215bf462f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c8c3de34-3aa0-4477-a489-1e9f85425120",
                    "LayerId": "0109caad-f954-4b24-94fc-0ee74c447707"
                }
            ]
        },
        {
            "id": "dfa6c514-749a-43f4-8d11-b54e795c11c0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c4963d42-e675-4704-b94c-fda4412e68c0",
            "compositeImage": {
                "id": "414a14f1-d837-4352-8f5b-195307cc718f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dfa6c514-749a-43f4-8d11-b54e795c11c0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "650a5b00-a9ca-4157-997c-a7b0b3c8db66",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dfa6c514-749a-43f4-8d11-b54e795c11c0",
                    "LayerId": "0109caad-f954-4b24-94fc-0ee74c447707"
                }
            ]
        },
        {
            "id": "e79aa50e-6abd-417a-8ac5-ee05720630e3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c4963d42-e675-4704-b94c-fda4412e68c0",
            "compositeImage": {
                "id": "4d32e738-29a3-452f-a429-32d523cdc0f1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e79aa50e-6abd-417a-8ac5-ee05720630e3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4c628845-5d55-4ec9-ba29-20dfdd50d616",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e79aa50e-6abd-417a-8ac5-ee05720630e3",
                    "LayerId": "0109caad-f954-4b24-94fc-0ee74c447707"
                }
            ]
        },
        {
            "id": "659951a0-f33a-4a76-acb2-7cde831439a1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c4963d42-e675-4704-b94c-fda4412e68c0",
            "compositeImage": {
                "id": "d8b1756b-51a8-4b11-8f91-7c99e82a73a5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "659951a0-f33a-4a76-acb2-7cde831439a1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c8f024c5-3966-4884-a658-4cda4ec20206",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "659951a0-f33a-4a76-acb2-7cde831439a1",
                    "LayerId": "0109caad-f954-4b24-94fc-0ee74c447707"
                }
            ]
        },
        {
            "id": "8deb20dd-7f08-4a61-8b0b-d9aab415ae4f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c4963d42-e675-4704-b94c-fda4412e68c0",
            "compositeImage": {
                "id": "a07eae1b-bb0f-4655-99c1-2f3176bacc7b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8deb20dd-7f08-4a61-8b0b-d9aab415ae4f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "214465ba-84df-46c9-97a2-3ff4f725657e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8deb20dd-7f08-4a61-8b0b-d9aab415ae4f",
                    "LayerId": "0109caad-f954-4b24-94fc-0ee74c447707"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "0109caad-f954-4b24-94fc-0ee74c447707",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c4963d42-e675-4704-b94c-fda4412e68c0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 35,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 44,
    "xorig": 22,
    "yorig": 12
}