{
    "id": "78b803d3-8f8f-4842-8c4c-146d5e6e2245",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spt_playerDead",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 24,
    "bbox_left": 1,
    "bbox_right": 14,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8408bbea-69e3-4879-9d88-c2afe3d2a586",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "78b803d3-8f8f-4842-8c4c-146d5e6e2245",
            "compositeImage": {
                "id": "a653cc29-cadd-404c-b03c-07667579d2ec",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8408bbea-69e3-4879-9d88-c2afe3d2a586",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "200216ea-c072-46bd-8df0-d318b22d3fa4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8408bbea-69e3-4879-9d88-c2afe3d2a586",
                    "LayerId": "a1962fd7-172d-44dc-82be-3b39add76beb"
                }
            ]
        },
        {
            "id": "98f19f47-b3e1-468e-915d-0218aec13356",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "78b803d3-8f8f-4842-8c4c-146d5e6e2245",
            "compositeImage": {
                "id": "797138e2-bdf0-4f34-8b16-1c9451eefef9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "98f19f47-b3e1-468e-915d-0218aec13356",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "30526812-295b-4147-a542-7a2569bd5ceb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "98f19f47-b3e1-468e-915d-0218aec13356",
                    "LayerId": "a1962fd7-172d-44dc-82be-3b39add76beb"
                }
            ]
        },
        {
            "id": "b1b3f7f1-ed1d-406c-baa4-4b7fa9740058",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "78b803d3-8f8f-4842-8c4c-146d5e6e2245",
            "compositeImage": {
                "id": "968b572c-75af-4a49-9786-cbaa1342dd5b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b1b3f7f1-ed1d-406c-baa4-4b7fa9740058",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5db2f542-2c19-4df3-9843-4caf04d96120",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b1b3f7f1-ed1d-406c-baa4-4b7fa9740058",
                    "LayerId": "a1962fd7-172d-44dc-82be-3b39add76beb"
                }
            ]
        },
        {
            "id": "4c2a1599-2684-441b-b3e2-51148230a57b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "78b803d3-8f8f-4842-8c4c-146d5e6e2245",
            "compositeImage": {
                "id": "095ff273-717f-4bbb-9fde-c0a72f4dca58",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4c2a1599-2684-441b-b3e2-51148230a57b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "581a8d96-c878-468b-9550-6803e00308ce",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4c2a1599-2684-441b-b3e2-51148230a57b",
                    "LayerId": "a1962fd7-172d-44dc-82be-3b39add76beb"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 25,
    "layers": [
        {
            "id": "a1962fd7-172d-44dc-82be-3b39add76beb",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "78b803d3-8f8f-4842-8c4c-146d5e6e2245",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 5,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 25,
    "xorig": 12,
    "yorig": 12
}