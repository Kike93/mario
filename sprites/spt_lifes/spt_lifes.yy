{
    "id": "d1d5e097-83e4-4abf-82ec-5f13e2cba757",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spt_lifes",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 6,
    "bbox_left": 0,
    "bbox_right": 6,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d4d642e6-5450-4c43-bcd8-782ecfcc6aa8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d1d5e097-83e4-4abf-82ec-5f13e2cba757",
            "compositeImage": {
                "id": "5acfa764-6a71-4022-bdf7-c8c5e5da36cb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d4d642e6-5450-4c43-bcd8-782ecfcc6aa8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0c6b3c52-70d2-416b-94d1-1cebb747c9f0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d4d642e6-5450-4c43-bcd8-782ecfcc6aa8",
                    "LayerId": "9503cc92-ba9c-4361-9125-d24059b8aaae"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 7,
    "layers": [
        {
            "id": "9503cc92-ba9c-4361-9125-d24059b8aaae",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d1d5e097-83e4-4abf-82ec-5f13e2cba757",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 7,
    "xorig": 0,
    "yorig": 0
}