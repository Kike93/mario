{
    "id": "3863b400-a9b3-4f67-9f47-4d747fa92619",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spt_enemyRight",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 11,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "cfa9c57a-1be7-4d9f-9a20-24a32a4ac9fb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3863b400-a9b3-4f67-9f47-4d747fa92619",
            "compositeImage": {
                "id": "a925461e-2204-45fd-aa4a-6bb12915f839",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cfa9c57a-1be7-4d9f-9a20-24a32a4ac9fb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7bb26b0a-99c2-4c65-98cf-b4ab80ad26be",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cfa9c57a-1be7-4d9f-9a20-24a32a4ac9fb",
                    "LayerId": "bf39ec21-65d1-44fe-9e2a-fe3c409bd647"
                }
            ]
        },
        {
            "id": "5b913e8e-4e8e-4e08-800f-6c04aee941b9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3863b400-a9b3-4f67-9f47-4d747fa92619",
            "compositeImage": {
                "id": "0c623915-6ad9-4c58-947e-3d7c5132bb1e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5b913e8e-4e8e-4e08-800f-6c04aee941b9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "93b0932f-2bec-4e47-b373-a7de349ac731",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5b913e8e-4e8e-4e08-800f-6c04aee941b9",
                    "LayerId": "bf39ec21-65d1-44fe-9e2a-fe3c409bd647"
                }
            ]
        },
        {
            "id": "0add3e0a-196b-4ced-abb2-30f3c32d6ae1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3863b400-a9b3-4f67-9f47-4d747fa92619",
            "compositeImage": {
                "id": "c9153921-89a2-44cd-9ffb-6b9a0ee7c5e9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0add3e0a-196b-4ced-abb2-30f3c32d6ae1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cf6de4ef-6c93-4c07-acdf-c61474f7409b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0add3e0a-196b-4ced-abb2-30f3c32d6ae1",
                    "LayerId": "bf39ec21-65d1-44fe-9e2a-fe3c409bd647"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "bf39ec21-65d1-44fe-9e2a-fe3c409bd647",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3863b400-a9b3-4f67-9f47-4d747fa92619",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 8,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 12,
    "xorig": 6,
    "yorig": 8
}