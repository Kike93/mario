{
    "id": "508ca79b-fa34-4501-9255-dddf0794a86f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spt_water",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 29,
    "bbox_left": 0,
    "bbox_right": 29,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8a13ce18-f437-4442-96a1-1b035adb79ee",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "508ca79b-fa34-4501-9255-dddf0794a86f",
            "compositeImage": {
                "id": "08c5401d-da30-4570-ade6-604544b0283b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8a13ce18-f437-4442-96a1-1b035adb79ee",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9e68e836-b5f8-4df1-a1df-4d4a9adb9c4a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8a13ce18-f437-4442-96a1-1b035adb79ee",
                    "LayerId": "14513004-9505-4f49-bf17-84b213afde91"
                }
            ]
        },
        {
            "id": "5910b800-e9a8-4956-85a3-d363d697f275",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "508ca79b-fa34-4501-9255-dddf0794a86f",
            "compositeImage": {
                "id": "d0d1aeed-74dd-4b82-9a2b-aaac98241251",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5910b800-e9a8-4956-85a3-d363d697f275",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bc5ed912-4be7-4b86-a2d7-c058ba7325d9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5910b800-e9a8-4956-85a3-d363d697f275",
                    "LayerId": "14513004-9505-4f49-bf17-84b213afde91"
                }
            ]
        },
        {
            "id": "a1bd8d56-0a06-40cd-b60f-3e7a89c6ed62",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "508ca79b-fa34-4501-9255-dddf0794a86f",
            "compositeImage": {
                "id": "e0baddbb-2624-49c3-9543-45b18036fb37",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a1bd8d56-0a06-40cd-b60f-3e7a89c6ed62",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "99db7918-3720-415d-ad4d-60fa9286e5c7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a1bd8d56-0a06-40cd-b60f-3e7a89c6ed62",
                    "LayerId": "14513004-9505-4f49-bf17-84b213afde91"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 30,
    "layers": [
        {
            "id": "14513004-9505-4f49-bf17-84b213afde91",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "508ca79b-fa34-4501-9255-dddf0794a86f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 6,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 30,
    "xorig": 15,
    "yorig": 15
}