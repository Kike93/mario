{
    "id": "f1777ece-7444-45c7-9161-365db7eeb954",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spt_playerRight",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 24,
    "bbox_left": 0,
    "bbox_right": 14,
    "bbox_top": 4,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "cd446b4a-148e-4cdd-bb5d-473d20c05439",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f1777ece-7444-45c7-9161-365db7eeb954",
            "compositeImage": {
                "id": "f287c2a2-4036-42f5-96de-26923a9250f4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cd446b4a-148e-4cdd-bb5d-473d20c05439",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6c396756-38c8-4534-9948-04e11e199d9d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cd446b4a-148e-4cdd-bb5d-473d20c05439",
                    "LayerId": "f946858e-108a-4ee3-bacf-3d480ba99cb6"
                }
            ]
        },
        {
            "id": "321003c1-95f7-4d14-b976-0a7617dc41d0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f1777ece-7444-45c7-9161-365db7eeb954",
            "compositeImage": {
                "id": "e8679aa8-4b63-4ef8-b28b-710a9ef40202",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "321003c1-95f7-4d14-b976-0a7617dc41d0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5c08b034-996b-47c0-b521-8ec5e9b8e0a8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "321003c1-95f7-4d14-b976-0a7617dc41d0",
                    "LayerId": "f946858e-108a-4ee3-bacf-3d480ba99cb6"
                }
            ]
        },
        {
            "id": "e56b1b33-f8dc-48a7-9fc0-6e286f1b1245",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f1777ece-7444-45c7-9161-365db7eeb954",
            "compositeImage": {
                "id": "4763a525-1100-4946-ace8-fda1db39b594",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e56b1b33-f8dc-48a7-9fc0-6e286f1b1245",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b95838d3-6f84-4031-9119-beba01f3184c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e56b1b33-f8dc-48a7-9fc0-6e286f1b1245",
                    "LayerId": "f946858e-108a-4ee3-bacf-3d480ba99cb6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 25,
    "layers": [
        {
            "id": "f946858e-108a-4ee3-bacf-3d480ba99cb6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f1777ece-7444-45c7-9161-365db7eeb954",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 25,
    "xorig": 12,
    "yorig": 12
}