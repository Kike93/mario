{
    "id": "cd58e6c8-443c-4015-b46e-f3bcd70b1627",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spt_wall2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7a71b212-85fc-41fb-815c-4b8a0c3b3b9e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cd58e6c8-443c-4015-b46e-f3bcd70b1627",
            "compositeImage": {
                "id": "0177a3eb-8b79-487a-9030-e3184ee1d492",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7a71b212-85fc-41fb-815c-4b8a0c3b3b9e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fabe4d25-c03c-4694-b6aa-f25e8d646136",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7a71b212-85fc-41fb-815c-4b8a0c3b3b9e",
                    "LayerId": "12dcde3d-ceac-429a-8d6b-0a3a2431b3e5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "12dcde3d-ceac-429a-8d6b-0a3a2431b3e5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "cd58e6c8-443c-4015-b46e-f3bcd70b1627",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": true,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}