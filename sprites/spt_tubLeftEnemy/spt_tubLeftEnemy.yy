{
    "id": "f74cc3fd-1dac-4984-b0ea-9ce36b169c3c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spt_tubLeftEnemy",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 0,
    "bbox_right": 43,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ad7ffbe7-6c8e-4e29-a040-73d15adaf301",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f74cc3fd-1dac-4984-b0ea-9ce36b169c3c",
            "compositeImage": {
                "id": "c87b5c66-6960-4ace-9ef2-692275d5ec61",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ad7ffbe7-6c8e-4e29-a040-73d15adaf301",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "905a738f-812e-4582-9f5d-afde228aa7e7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ad7ffbe7-6c8e-4e29-a040-73d15adaf301",
                    "LayerId": "929fddf7-920e-40f0-a54a-db2c77c2a8c3"
                }
            ]
        },
        {
            "id": "86e381b5-3715-46da-b63f-eed619caaa45",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f74cc3fd-1dac-4984-b0ea-9ce36b169c3c",
            "compositeImage": {
                "id": "89ac7010-7743-4924-91b8-3e8802535175",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "86e381b5-3715-46da-b63f-eed619caaa45",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "45df7657-6116-4c77-903b-ef2d61289f69",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "86e381b5-3715-46da-b63f-eed619caaa45",
                    "LayerId": "929fddf7-920e-40f0-a54a-db2c77c2a8c3"
                }
            ]
        },
        {
            "id": "ddd08a60-9759-421f-b5cd-2599b9639826",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f74cc3fd-1dac-4984-b0ea-9ce36b169c3c",
            "compositeImage": {
                "id": "6b259df6-704c-4171-b59c-daea4cdd692a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ddd08a60-9759-421f-b5cd-2599b9639826",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ff3370ff-112e-4b6f-882f-e70b404b4ccc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ddd08a60-9759-421f-b5cd-2599b9639826",
                    "LayerId": "929fddf7-920e-40f0-a54a-db2c77c2a8c3"
                }
            ]
        },
        {
            "id": "f5ea9444-62db-435f-b495-a05cb7d70a22",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f74cc3fd-1dac-4984-b0ea-9ce36b169c3c",
            "compositeImage": {
                "id": "253f9103-d72c-4016-be88-576ac0437346",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f5ea9444-62db-435f-b495-a05cb7d70a22",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "12cc0dc0-c773-4f19-a254-8a21c167ff1d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f5ea9444-62db-435f-b495-a05cb7d70a22",
                    "LayerId": "929fddf7-920e-40f0-a54a-db2c77c2a8c3"
                }
            ]
        },
        {
            "id": "f7bae407-0766-46fa-9a49-725bee6a1c7f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f74cc3fd-1dac-4984-b0ea-9ce36b169c3c",
            "compositeImage": {
                "id": "d98ba3d1-33bd-44bf-945a-d4afbb0e366c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f7bae407-0766-46fa-9a49-725bee6a1c7f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f2b528ba-f8ae-4fde-805e-aeb79856aea8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f7bae407-0766-46fa-9a49-725bee6a1c7f",
                    "LayerId": "929fddf7-920e-40f0-a54a-db2c77c2a8c3"
                }
            ]
        },
        {
            "id": "ae82c2e0-41de-4b59-bdd1-abfbc6d34251",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f74cc3fd-1dac-4984-b0ea-9ce36b169c3c",
            "compositeImage": {
                "id": "da77c56a-f84c-4949-8d69-881b6773fe0b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ae82c2e0-41de-4b59-bdd1-abfbc6d34251",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8378e19a-42ac-458a-9489-a011067f317c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ae82c2e0-41de-4b59-bdd1-abfbc6d34251",
                    "LayerId": "929fddf7-920e-40f0-a54a-db2c77c2a8c3"
                }
            ]
        },
        {
            "id": "f450a398-4d92-41e7-bb45-bfb56c0ff590",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f74cc3fd-1dac-4984-b0ea-9ce36b169c3c",
            "compositeImage": {
                "id": "7b9c911a-903d-48ef-a02c-8b139115c246",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f450a398-4d92-41e7-bb45-bfb56c0ff590",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c864b053-501e-429e-b7ed-062320f2fe7d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f450a398-4d92-41e7-bb45-bfb56c0ff590",
                    "LayerId": "929fddf7-920e-40f0-a54a-db2c77c2a8c3"
                }
            ]
        },
        {
            "id": "6b36fa82-c076-4815-9441-c3be20c3b7d8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f74cc3fd-1dac-4984-b0ea-9ce36b169c3c",
            "compositeImage": {
                "id": "72473939-9389-4ccd-9e16-2b5f4402f515",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6b36fa82-c076-4815-9441-c3be20c3b7d8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "23b4d880-1262-4359-968a-d42f825445d4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6b36fa82-c076-4815-9441-c3be20c3b7d8",
                    "LayerId": "929fddf7-920e-40f0-a54a-db2c77c2a8c3"
                }
            ]
        },
        {
            "id": "00a1c903-4013-40f1-a4e5-60dcbeb00253",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f74cc3fd-1dac-4984-b0ea-9ce36b169c3c",
            "compositeImage": {
                "id": "efeeb176-f0ef-4fc5-94b8-51979f7ee471",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "00a1c903-4013-40f1-a4e5-60dcbeb00253",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "15aca0a4-c8fd-4b18-9eb9-9e3d25dd871c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "00a1c903-4013-40f1-a4e5-60dcbeb00253",
                    "LayerId": "929fddf7-920e-40f0-a54a-db2c77c2a8c3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "929fddf7-920e-40f0-a54a-db2c77c2a8c3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f74cc3fd-1dac-4984-b0ea-9ce36b169c3c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 35,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 44,
    "xorig": 22,
    "yorig": 12
}