{
    "id": "acececf4-d599-4fad-bdc9-01bdc54332a4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spt_enemy2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 14,
    "bbox_left": 0,
    "bbox_right": 13,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c22f0d66-c144-4225-99c8-3008b71c79aa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "acececf4-d599-4fad-bdc9-01bdc54332a4",
            "compositeImage": {
                "id": "fe2cc0d3-707a-4493-bef5-b23046e49b1b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c22f0d66-c144-4225-99c8-3008b71c79aa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cae205f1-65e9-4c58-8746-5733f04297bd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c22f0d66-c144-4225-99c8-3008b71c79aa",
                    "LayerId": "b5fde447-a695-4742-9dba-02c285e5906d"
                }
            ]
        },
        {
            "id": "90c9abac-c501-4075-9ae4-c1799325a1a9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "acececf4-d599-4fad-bdc9-01bdc54332a4",
            "compositeImage": {
                "id": "4a218bc9-ed37-4f9c-9458-93ac5103f303",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "90c9abac-c501-4075-9ae4-c1799325a1a9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "62e6e89d-07d0-4bc8-9ad2-d99d1501d9aa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "90c9abac-c501-4075-9ae4-c1799325a1a9",
                    "LayerId": "b5fde447-a695-4742-9dba-02c285e5906d"
                }
            ]
        },
        {
            "id": "ba9786bb-7391-4648-a0a3-4075378d11e5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "acececf4-d599-4fad-bdc9-01bdc54332a4",
            "compositeImage": {
                "id": "77488355-5623-455d-aa51-dba9428d1e4b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ba9786bb-7391-4648-a0a3-4075378d11e5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fb4d424a-6625-4fed-aaf6-7f49c0c3fc51",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ba9786bb-7391-4648-a0a3-4075378d11e5",
                    "LayerId": "b5fde447-a695-4742-9dba-02c285e5906d"
                }
            ]
        },
        {
            "id": "2e9b96fa-8fb0-4876-a8fc-74ba589aecf5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "acececf4-d599-4fad-bdc9-01bdc54332a4",
            "compositeImage": {
                "id": "cbabacab-b138-49e0-a5e3-a55720388f2a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2e9b96fa-8fb0-4876-a8fc-74ba589aecf5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8ddf7d55-e11d-4cb3-ba15-0cb885cbb3e3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2e9b96fa-8fb0-4876-a8fc-74ba589aecf5",
                    "LayerId": "b5fde447-a695-4742-9dba-02c285e5906d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 15,
    "layers": [
        {
            "id": "b5fde447-a695-4742-9dba-02c285e5906d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "acececf4-d599-4fad-bdc9-01bdc54332a4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 4,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 14,
    "xorig": 7,
    "yorig": 7
}