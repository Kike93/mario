{
    "id": "0d79ac94-4590-44cb-aae0-0a172cb3d161",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spt_playerStayLeft",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 24,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 3,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2509143b-42c2-402f-ba9c-17ac4db2a712",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0d79ac94-4590-44cb-aae0-0a172cb3d161",
            "compositeImage": {
                "id": "1eff95dd-b6d0-494b-ae70-4bd4f709c037",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2509143b-42c2-402f-ba9c-17ac4db2a712",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1b463f37-1d28-4e4e-8170-838799614eaf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2509143b-42c2-402f-ba9c-17ac4db2a712",
                    "LayerId": "d3c29343-2146-4173-8217-8003ee49679a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 25,
    "layers": [
        {
            "id": "d3c29343-2146-4173-8217-8003ee49679a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0d79ac94-4590-44cb-aae0-0a172cb3d161",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 6,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 25,
    "xorig": 12,
    "yorig": 12
}