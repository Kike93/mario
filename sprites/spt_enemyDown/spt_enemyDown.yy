{
    "id": "2862c4a1-0803-4a06-83fa-4bee2e57ba74",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spt_enemyDown",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 11,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8a0daae4-86fd-4f74-92b0-5ba220c7fa34",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2862c4a1-0803-4a06-83fa-4bee2e57ba74",
            "compositeImage": {
                "id": "59e3a78f-c225-49b6-aa61-98eb4a1b1562",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8a0daae4-86fd-4f74-92b0-5ba220c7fa34",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "efc679ef-6a8f-47c0-a7c9-f1a5c0d0f3ed",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8a0daae4-86fd-4f74-92b0-5ba220c7fa34",
                    "LayerId": "2dbe8459-ee55-4f94-b7d7-0999855c7be4"
                }
            ]
        },
        {
            "id": "b6558b4d-fc6b-4d60-b7a8-700c7fd02156",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2862c4a1-0803-4a06-83fa-4bee2e57ba74",
            "compositeImage": {
                "id": "a74b3b02-9a83-4d80-87d3-875615ecb574",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b6558b4d-fc6b-4d60-b7a8-700c7fd02156",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "282323ca-235a-42ce-b980-e650a09bc3be",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b6558b4d-fc6b-4d60-b7a8-700c7fd02156",
                    "LayerId": "2dbe8459-ee55-4f94-b7d7-0999855c7be4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "2dbe8459-ee55-4f94-b7d7-0999855c7be4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2862c4a1-0803-4a06-83fa-4bee2e57ba74",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 2,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 12,
    "xorig": 6,
    "yorig": 8
}