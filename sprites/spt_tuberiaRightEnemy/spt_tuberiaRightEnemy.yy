{
    "id": "14fea20d-be21-4f21-a4d3-e2f55a7260f5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spt_tuberiaRightEnemy",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 27,
    "bbox_left": 0,
    "bbox_right": 51,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5df7deaf-f913-42a6-bb4b-3f2f2a092665",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "14fea20d-be21-4f21-a4d3-e2f55a7260f5",
            "compositeImage": {
                "id": "4ac409c1-fbe5-4b55-890e-553f42d6fbb8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5df7deaf-f913-42a6-bb4b-3f2f2a092665",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "448a18ae-4665-42d6-9eaf-45fcc6d172ae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5df7deaf-f913-42a6-bb4b-3f2f2a092665",
                    "LayerId": "74d931dd-0d8e-4bf9-832c-94b458c9cb3f"
                }
            ]
        },
        {
            "id": "a3bb4252-b4f8-4e60-a67a-a048bba365ac",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "14fea20d-be21-4f21-a4d3-e2f55a7260f5",
            "compositeImage": {
                "id": "dfc510dd-3b22-4ba1-aa76-149739f3afea",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a3bb4252-b4f8-4e60-a67a-a048bba365ac",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ae64f439-ce4d-4249-ad83-3d50e65887e1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a3bb4252-b4f8-4e60-a67a-a048bba365ac",
                    "LayerId": "74d931dd-0d8e-4bf9-832c-94b458c9cb3f"
                }
            ]
        },
        {
            "id": "02311356-5297-49db-a75b-eefb6a659cae",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "14fea20d-be21-4f21-a4d3-e2f55a7260f5",
            "compositeImage": {
                "id": "a173ecf7-7526-47c9-9e96-b6b7c460a388",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "02311356-5297-49db-a75b-eefb6a659cae",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cdc73e40-c261-4186-b83e-3ea9b8fa9adc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "02311356-5297-49db-a75b-eefb6a659cae",
                    "LayerId": "74d931dd-0d8e-4bf9-832c-94b458c9cb3f"
                }
            ]
        },
        {
            "id": "5a27451c-ec78-49de-96a6-a509223c1bf0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "14fea20d-be21-4f21-a4d3-e2f55a7260f5",
            "compositeImage": {
                "id": "2a123029-8774-4786-9ebf-c8e8312793b9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5a27451c-ec78-49de-96a6-a509223c1bf0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "42753049-f617-417b-b523-6205b0d52d82",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5a27451c-ec78-49de-96a6-a509223c1bf0",
                    "LayerId": "74d931dd-0d8e-4bf9-832c-94b458c9cb3f"
                }
            ]
        },
        {
            "id": "5e8a6844-7e46-4950-8de7-06f2d5045294",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "14fea20d-be21-4f21-a4d3-e2f55a7260f5",
            "compositeImage": {
                "id": "343c0460-95ed-4e6b-b899-fda7dd43da5e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5e8a6844-7e46-4950-8de7-06f2d5045294",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9b128a58-4fcc-48cc-9fa7-922881529f43",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5e8a6844-7e46-4950-8de7-06f2d5045294",
                    "LayerId": "74d931dd-0d8e-4bf9-832c-94b458c9cb3f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 28,
    "layers": [
        {
            "id": "74d931dd-0d8e-4bf9-832c-94b458c9cb3f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "14fea20d-be21-4f21-a4d3-e2f55a7260f5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 55,
    "xorig": 27,
    "yorig": 14
}