{
    "id": "1a808559-d70c-4b95-92b0-28d02dce8e50",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spt_tuberiaRight",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 27,
    "bbox_left": 0,
    "bbox_right": 39,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8b03d692-0337-4655-872e-bc198fca2b30",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1a808559-d70c-4b95-92b0-28d02dce8e50",
            "compositeImage": {
                "id": "6b51a0bb-46c8-421c-964b-d4b2e795bf42",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8b03d692-0337-4655-872e-bc198fca2b30",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "50bf78b8-d12e-46c1-a486-41e53aea488b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8b03d692-0337-4655-872e-bc198fca2b30",
                    "LayerId": "0331d279-8993-4b5f-80e8-840559c70745"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 28,
    "layers": [
        {
            "id": "0331d279-8993-4b5f-80e8-840559c70745",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1a808559-d70c-4b95-92b0-28d02dce8e50",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 55,
    "xorig": 27,
    "yorig": 14
}