{
    "id": "95038ad8-1a8f-4682-969e-fe64f1807fe7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spt_playerLeft",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 24,
    "bbox_left": 0,
    "bbox_right": 14,
    "bbox_top": 4,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "98d572c6-717d-4215-a5bb-fcc29e5e38ae",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "95038ad8-1a8f-4682-969e-fe64f1807fe7",
            "compositeImage": {
                "id": "f57fca99-7120-4936-9b26-4a25375745c7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "98d572c6-717d-4215-a5bb-fcc29e5e38ae",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "62a93169-f3fa-4357-a838-e6a898051e70",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "98d572c6-717d-4215-a5bb-fcc29e5e38ae",
                    "LayerId": "d2b957b7-75f1-4775-845a-472191c93ec9"
                }
            ]
        },
        {
            "id": "6f2a4f6d-a7f2-47ab-8b09-349c70a99d96",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "95038ad8-1a8f-4682-969e-fe64f1807fe7",
            "compositeImage": {
                "id": "1cdbf34e-8b4f-4407-9a8d-30c915c92b1e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6f2a4f6d-a7f2-47ab-8b09-349c70a99d96",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "12948e5b-6f02-4c10-9fb1-62c791c4990f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6f2a4f6d-a7f2-47ab-8b09-349c70a99d96",
                    "LayerId": "d2b957b7-75f1-4775-845a-472191c93ec9"
                }
            ]
        },
        {
            "id": "b35853d6-9409-4119-b562-b1d5a3e40142",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "95038ad8-1a8f-4682-969e-fe64f1807fe7",
            "compositeImage": {
                "id": "08ba92c1-fd6e-4acf-b285-f369684893b5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b35853d6-9409-4119-b562-b1d5a3e40142",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "29da6b0d-6729-40b1-a8cc-fb27462f3df1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b35853d6-9409-4119-b562-b1d5a3e40142",
                    "LayerId": "d2b957b7-75f1-4775-845a-472191c93ec9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 25,
    "layers": [
        {
            "id": "d2b957b7-75f1-4775-845a-472191c93ec9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "95038ad8-1a8f-4682-969e-fe64f1807fe7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 25,
    "xorig": 12,
    "yorig": 12
}