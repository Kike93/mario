{
    "id": "2f43332b-126d-4247-affc-a984ccfa9b2d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spt_animCoin",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 8,
    "bbox_left": 0,
    "bbox_right": 6,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "cd4ed2ba-6610-4050-af11-c3de8911690f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2f43332b-126d-4247-affc-a984ccfa9b2d",
            "compositeImage": {
                "id": "7b113aed-9536-41a0-95c4-02917e2b9c4e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cd4ed2ba-6610-4050-af11-c3de8911690f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dd9bcadd-1a1d-428b-8389-c8e0b092fc05",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cd4ed2ba-6610-4050-af11-c3de8911690f",
                    "LayerId": "873efd60-ccb3-4781-b64b-7a5ee3814045"
                }
            ]
        },
        {
            "id": "0c4285ae-43ef-4201-83dc-ee148ed16b2e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2f43332b-126d-4247-affc-a984ccfa9b2d",
            "compositeImage": {
                "id": "d0a3edd7-234d-4534-97c5-d74dcfc4fb4a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0c4285ae-43ef-4201-83dc-ee148ed16b2e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "981b944f-f26e-41ff-939f-190cd3146c17",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0c4285ae-43ef-4201-83dc-ee148ed16b2e",
                    "LayerId": "873efd60-ccb3-4781-b64b-7a5ee3814045"
                }
            ]
        },
        {
            "id": "9076af36-247e-4b93-935b-12f8e624bd4c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2f43332b-126d-4247-affc-a984ccfa9b2d",
            "compositeImage": {
                "id": "bf1c8985-ed6e-4a5c-ab25-aee3c3aba6c0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9076af36-247e-4b93-935b-12f8e624bd4c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e0736acb-09be-4bef-be69-bc0c66871016",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9076af36-247e-4b93-935b-12f8e624bd4c",
                    "LayerId": "873efd60-ccb3-4781-b64b-7a5ee3814045"
                }
            ]
        },
        {
            "id": "896f2c94-4fe3-4394-868a-4480877abca1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2f43332b-126d-4247-affc-a984ccfa9b2d",
            "compositeImage": {
                "id": "2824d28a-6417-4d1f-b8b5-4aef89d00704",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "896f2c94-4fe3-4394-868a-4480877abca1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "71705bc5-15d0-49b2-bbbe-1026fed3fac5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "896f2c94-4fe3-4394-868a-4480877abca1",
                    "LayerId": "873efd60-ccb3-4781-b64b-7a5ee3814045"
                }
            ]
        },
        {
            "id": "1371872e-230d-48dc-9d01-e221da2fe6d9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2f43332b-126d-4247-affc-a984ccfa9b2d",
            "compositeImage": {
                "id": "96ba8f39-95ad-43ff-87cd-3b9d396b0ef4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1371872e-230d-48dc-9d01-e221da2fe6d9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "074108d5-7128-4b67-a698-e353d8b998e2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1371872e-230d-48dc-9d01-e221da2fe6d9",
                    "LayerId": "873efd60-ccb3-4781-b64b-7a5ee3814045"
                }
            ]
        },
        {
            "id": "0498510e-cd70-43f8-afc6-66f9c4a092fe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2f43332b-126d-4247-affc-a984ccfa9b2d",
            "compositeImage": {
                "id": "cde38c81-6fc1-4cda-9904-1b0763ad9d59",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0498510e-cd70-43f8-afc6-66f9c4a092fe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c09c2c3a-67d3-42aa-825f-1f1fa16eb5c1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0498510e-cd70-43f8-afc6-66f9c4a092fe",
                    "LayerId": "873efd60-ccb3-4781-b64b-7a5ee3814045"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 9,
    "layers": [
        {
            "id": "873efd60-ccb3-4781-b64b-7a5ee3814045",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2f43332b-126d-4247-affc-a984ccfa9b2d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 5,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 7,
    "xorig": 3,
    "yorig": 4
}