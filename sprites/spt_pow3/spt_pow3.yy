{
    "id": "d9c0880e-fabd-4a3e-b3e9-89655247c6e4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spt_pow3",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 8,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "fccf1ba5-f17c-4a1d-9968-ca030514a9c1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d9c0880e-fabd-4a3e-b3e9-89655247c6e4",
            "compositeImage": {
                "id": "8a2e9fe9-b764-4c2f-a452-85dde6565a4d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fccf1ba5-f17c-4a1d-9968-ca030514a9c1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "209580e0-652a-4623-886d-af951267176a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fccf1ba5-f17c-4a1d-9968-ca030514a9c1",
                    "LayerId": "770a4fc2-f49d-467b-957f-3121f7ffb9e9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 9,
    "layers": [
        {
            "id": "770a4fc2-f49d-467b-957f-3121f7ffb9e9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d9c0880e-fabd-4a3e-b3e9-89655247c6e4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 4
}