{
    "id": "12b2f0ff-8e6a-4b07-b13a-a4e3ac8e1da9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spt_tubRightEnemy",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 0,
    "bbox_right": 43,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e5beb992-216a-4218-8312-47ebe80e1b81",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "12b2f0ff-8e6a-4b07-b13a-a4e3ac8e1da9",
            "compositeImage": {
                "id": "73e83995-5805-463d-b80e-1e10ff6f8bd8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e5beb992-216a-4218-8312-47ebe80e1b81",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b3f31e24-1a3b-4cda-88e1-051cfc089aea",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e5beb992-216a-4218-8312-47ebe80e1b81",
                    "LayerId": "5a2a6296-3e3a-48a8-b21a-73b40651c693"
                }
            ]
        },
        {
            "id": "2fb6123e-97d6-4d8a-ac2a-317aa40d6ddb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "12b2f0ff-8e6a-4b07-b13a-a4e3ac8e1da9",
            "compositeImage": {
                "id": "bcbd52e6-149b-47e5-a77d-6f546568f435",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2fb6123e-97d6-4d8a-ac2a-317aa40d6ddb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "731982ce-856d-4c6d-a964-60d33e25a092",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2fb6123e-97d6-4d8a-ac2a-317aa40d6ddb",
                    "LayerId": "5a2a6296-3e3a-48a8-b21a-73b40651c693"
                }
            ]
        },
        {
            "id": "d02cfb61-8330-416f-87d0-898c92051c7c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "12b2f0ff-8e6a-4b07-b13a-a4e3ac8e1da9",
            "compositeImage": {
                "id": "73f30d77-96af-4d6a-bbcc-73eeb5975435",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d02cfb61-8330-416f-87d0-898c92051c7c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2f978367-df71-47f7-b5be-ac0c4fdf711c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d02cfb61-8330-416f-87d0-898c92051c7c",
                    "LayerId": "5a2a6296-3e3a-48a8-b21a-73b40651c693"
                }
            ]
        },
        {
            "id": "2a1625e2-88f3-4ae2-8e4d-12e2b6a3e54f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "12b2f0ff-8e6a-4b07-b13a-a4e3ac8e1da9",
            "compositeImage": {
                "id": "5085aeec-76a3-4931-86d3-c08880186d18",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2a1625e2-88f3-4ae2-8e4d-12e2b6a3e54f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b0fdd4d7-f01b-4412-b888-6ce2117958e3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2a1625e2-88f3-4ae2-8e4d-12e2b6a3e54f",
                    "LayerId": "5a2a6296-3e3a-48a8-b21a-73b40651c693"
                }
            ]
        },
        {
            "id": "81b26f2c-6857-4f74-bb1f-c9936298612c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "12b2f0ff-8e6a-4b07-b13a-a4e3ac8e1da9",
            "compositeImage": {
                "id": "cd3fceb1-f61a-434e-b983-5d4fac522547",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "81b26f2c-6857-4f74-bb1f-c9936298612c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7b03fce8-59af-411b-8ecb-1e8715e5a44b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "81b26f2c-6857-4f74-bb1f-c9936298612c",
                    "LayerId": "5a2a6296-3e3a-48a8-b21a-73b40651c693"
                }
            ]
        },
        {
            "id": "3ade3fed-2143-42f5-94ce-a7ac407e58fc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "12b2f0ff-8e6a-4b07-b13a-a4e3ac8e1da9",
            "compositeImage": {
                "id": "259de11d-1d84-4439-b101-194e2e90d594",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3ade3fed-2143-42f5-94ce-a7ac407e58fc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3ec2f595-67a9-413f-afaf-82596383e8b6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3ade3fed-2143-42f5-94ce-a7ac407e58fc",
                    "LayerId": "5a2a6296-3e3a-48a8-b21a-73b40651c693"
                }
            ]
        },
        {
            "id": "a1028736-b54e-4f07-a893-46c5f930cd9f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "12b2f0ff-8e6a-4b07-b13a-a4e3ac8e1da9",
            "compositeImage": {
                "id": "7fe19973-d2df-44ca-809a-e460d7b7100d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a1028736-b54e-4f07-a893-46c5f930cd9f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3acf4cae-5ae4-4852-a1af-84ac8510600d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a1028736-b54e-4f07-a893-46c5f930cd9f",
                    "LayerId": "5a2a6296-3e3a-48a8-b21a-73b40651c693"
                }
            ]
        },
        {
            "id": "8958b25c-1b17-445e-beff-461a9a76ef58",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "12b2f0ff-8e6a-4b07-b13a-a4e3ac8e1da9",
            "compositeImage": {
                "id": "551e5aae-5911-41c9-8dc2-5fb13c19365a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8958b25c-1b17-445e-beff-461a9a76ef58",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "76ef2501-54f1-4a18-a0e2-cc8012383ca9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8958b25c-1b17-445e-beff-461a9a76ef58",
                    "LayerId": "5a2a6296-3e3a-48a8-b21a-73b40651c693"
                }
            ]
        },
        {
            "id": "a028bef2-0f86-4e59-a311-187b85f40fee",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "12b2f0ff-8e6a-4b07-b13a-a4e3ac8e1da9",
            "compositeImage": {
                "id": "528a8aed-5b56-451a-947d-2d6399dcd3da",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a028bef2-0f86-4e59-a311-187b85f40fee",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "26330bee-cab1-440e-a81e-80f937a4c514",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a028bef2-0f86-4e59-a311-187b85f40fee",
                    "LayerId": "5a2a6296-3e3a-48a8-b21a-73b40651c693"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "5a2a6296-3e3a-48a8-b21a-73b40651c693",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "12b2f0ff-8e6a-4b07-b13a-a4e3ac8e1da9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 35,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 44,
    "xorig": 22,
    "yorig": 12
}