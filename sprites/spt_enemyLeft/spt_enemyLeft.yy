{
    "id": "458c4edc-feac-4132-90fb-77b5258a3ebc",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spt_enemyLeft",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 11,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e2186272-219f-40d9-a736-e52a97ea6ef5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "458c4edc-feac-4132-90fb-77b5258a3ebc",
            "compositeImage": {
                "id": "9e82355f-d19a-4c99-ba77-d794557ec21f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e2186272-219f-40d9-a736-e52a97ea6ef5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5340e6b1-0ec6-47ce-bb5c-790054666fd9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e2186272-219f-40d9-a736-e52a97ea6ef5",
                    "LayerId": "f1cf3032-6a74-4384-ba94-1a7831a54c18"
                }
            ]
        },
        {
            "id": "bd65dc1e-24f1-4dbc-932d-e176c68c6916",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "458c4edc-feac-4132-90fb-77b5258a3ebc",
            "compositeImage": {
                "id": "22e3b3ac-4045-4e8f-8258-ffcaa5659e47",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bd65dc1e-24f1-4dbc-932d-e176c68c6916",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9ef6a45b-8205-4ae4-b846-f55b3a49c09d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bd65dc1e-24f1-4dbc-932d-e176c68c6916",
                    "LayerId": "f1cf3032-6a74-4384-ba94-1a7831a54c18"
                }
            ]
        },
        {
            "id": "c11b2082-b979-4bb8-a2f0-0ba98c22351d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "458c4edc-feac-4132-90fb-77b5258a3ebc",
            "compositeImage": {
                "id": "4e1f0074-1911-4084-98f4-78d2bfca4a8e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c11b2082-b979-4bb8-a2f0-0ba98c22351d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "014f2ee9-45a6-4313-a89d-0fc22a6e1056",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c11b2082-b979-4bb8-a2f0-0ba98c22351d",
                    "LayerId": "f1cf3032-6a74-4384-ba94-1a7831a54c18"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "f1cf3032-6a74-4384-ba94-1a7831a54c18",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "458c4edc-feac-4132-90fb-77b5258a3ebc",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 8,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 12,
    "xorig": 6,
    "yorig": 8
}