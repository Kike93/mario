{
    "id": "3c5931d0-14af-4f49-a1fc-ed2defa80783",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spt_wallAnimation",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 0,
    "bbox_right": 6,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "01b3f518-1c86-4862-afe9-7d325f9ae7e3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3c5931d0-14af-4f49-a1fc-ed2defa80783",
            "compositeImage": {
                "id": "58f49478-cf64-48f1-b284-12833479dd5e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "01b3f518-1c86-4862-afe9-7d325f9ae7e3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7eb6abbf-0c89-4f1c-a9c0-93b889160f20",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "01b3f518-1c86-4862-afe9-7d325f9ae7e3",
                    "LayerId": "037999f0-7876-4051-b444-958a91c693cb"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "037999f0-7876-4051-b444-958a91c693cb",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3c5931d0-14af-4f49-a1fc-ed2defa80783",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 7,
    "xorig": 0,
    "yorig": 0
}