{
    "id": "589026d4-cf6b-4f70-97f2-f263e5d3924d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spt_tubLeft",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 17,
    "bbox_left": 12,
    "bbox_right": 43,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ccfb4e4a-f4e3-4adc-9446-bdd8e6518fa2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "589026d4-cf6b-4f70-97f2-f263e5d3924d",
            "compositeImage": {
                "id": "5c70de5a-17ab-4633-bdf3-38f32de649a3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ccfb4e4a-f4e3-4adc-9446-bdd8e6518fa2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "897b2191-5b8d-4022-ac54-3a1214219cee",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ccfb4e4a-f4e3-4adc-9446-bdd8e6518fa2",
                    "LayerId": "a1111aba-bd9d-445d-9ee0-63c2e1c3b262"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "a1111aba-bd9d-445d-9ee0-63c2e1c3b262",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "589026d4-cf6b-4f70-97f2-f263e5d3924d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 44,
    "xorig": 22,
    "yorig": 12
}