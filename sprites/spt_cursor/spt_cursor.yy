{
    "id": "38dbacd1-e67f-430e-810e-2e6a15e944e6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spt_cursor",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 6,
    "bbox_left": 0,
    "bbox_right": 6,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a4901690-5498-4f6c-81c0-163eba4a4655",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "38dbacd1-e67f-430e-810e-2e6a15e944e6",
            "compositeImage": {
                "id": "d9fa4cfd-b18a-4d71-8e2d-e2fbf54654ef",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a4901690-5498-4f6c-81c0-163eba4a4655",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7ec85ab4-4e72-43de-8bea-8cb350640104",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a4901690-5498-4f6c-81c0-163eba4a4655",
                    "LayerId": "7fc65301-6122-440f-9a76-23f7c50b23b1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 7,
    "layers": [
        {
            "id": "7fc65301-6122-440f-9a76-23f7c50b23b1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "38dbacd1-e67f-430e-810e-2e6a15e944e6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 7,
    "xorig": 3,
    "yorig": 3
}