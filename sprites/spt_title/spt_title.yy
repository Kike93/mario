{
    "id": "867b1332-1fa8-48f0-82c4-25c421fae081",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spt_title",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 82,
    "bbox_left": 0,
    "bbox_right": 209,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a35f891e-f5e1-4e7a-a204-136ddf7b0e82",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "867b1332-1fa8-48f0-82c4-25c421fae081",
            "compositeImage": {
                "id": "474b3d43-d005-4015-90c7-07e1af09e8a6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a35f891e-f5e1-4e7a-a204-136ddf7b0e82",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4cc9bf65-45ef-4969-a081-2a6bbc32db6c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a35f891e-f5e1-4e7a-a204-136ddf7b0e82",
                    "LayerId": "efa291d0-6255-4c9b-8f83-f02a979f26e5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 83,
    "layers": [
        {
            "id": "efa291d0-6255-4c9b-8f83-f02a979f26e5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "867b1332-1fa8-48f0-82c4-25c421fae081",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 210,
    "xorig": 105,
    "yorig": 41
}