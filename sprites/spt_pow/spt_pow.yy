{
    "id": "16c4fd08-801a-4318-8007-2db642af41f3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spt_pow",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "dae35528-736e-4a26-8b14-bb50b7d3f5fb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "16c4fd08-801a-4318-8007-2db642af41f3",
            "compositeImage": {
                "id": "f5480a00-db44-43b6-b9a2-cb5ec9dd4fc5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dae35528-736e-4a26-8b14-bb50b7d3f5fb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0bd9b741-03ed-44b5-8357-3ab25d16e5b7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dae35528-736e-4a26-8b14-bb50b7d3f5fb",
                    "LayerId": "a292d352-d07f-4053-9da7-357668f77c08"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "a292d352-d07f-4053-9da7-357668f77c08",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "16c4fd08-801a-4318-8007-2db642af41f3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}