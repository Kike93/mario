{
    "id": "e44a6a1f-f28f-4e65-bf21-55fc5c0c4441",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spt_base",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 14,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2aef0b69-8523-4e58-acd0-33871d6386e9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e44a6a1f-f28f-4e65-bf21-55fc5c0c4441",
            "compositeImage": {
                "id": "cae96610-c811-41e5-9d2f-7e7b1bb53e95",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2aef0b69-8523-4e58-acd0-33871d6386e9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ab6efee8-711b-4c6a-84c4-a4a5729966cf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2aef0b69-8523-4e58-acd0-33871d6386e9",
                    "LayerId": "259d4294-bd76-4969-b5f8-c912c759256f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 15,
    "layers": [
        {
            "id": "259d4294-bd76-4969-b5f8-c912c759256f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e44a6a1f-f28f-4e65-bf21-55fc5c0c4441",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": true,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 7
}