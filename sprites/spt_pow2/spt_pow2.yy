{
    "id": "b5bd8384-638e-42ca-9992-28ee4af018d7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spt_pow2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 12,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c47b5048-32d4-4ebb-b837-1c02bb789ff7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b5bd8384-638e-42ca-9992-28ee4af018d7",
            "compositeImage": {
                "id": "45ffa533-9eb8-4a44-8d95-7d2f1f0e2955",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c47b5048-32d4-4ebb-b837-1c02bb789ff7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0ecacdf2-4b80-4fd7-a260-ff737fe3cafc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c47b5048-32d4-4ebb-b837-1c02bb789ff7",
                    "LayerId": "c6f43a2e-5c75-4431-83fb-8dd11f8cee80"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 13,
    "layers": [
        {
            "id": "c6f43a2e-5c75-4431-83fb-8dd11f8cee80",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b5bd8384-638e-42ca-9992-28ee4af018d7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 6
}