{
    "id": "63f25a1e-5035-4e83-9bbc-47fad97924e4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spt_tuberiaLeft",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 27,
    "bbox_left": 15,
    "bbox_right": 54,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "80baaa39-3a9b-448c-a342-af909743880e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "63f25a1e-5035-4e83-9bbc-47fad97924e4",
            "compositeImage": {
                "id": "a72884a7-d4e1-4882-9d24-6f36af321f30",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "80baaa39-3a9b-448c-a342-af909743880e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4c597ccc-de08-455f-b3fc-de25d63167cc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "80baaa39-3a9b-448c-a342-af909743880e",
                    "LayerId": "76791ba6-4923-4d4d-9c08-9fa9801eb6e7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 28,
    "layers": [
        {
            "id": "76791ba6-4923-4d4d-9c08-9fa9801eb6e7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "63f25a1e-5035-4e83-9bbc-47fad97924e4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 55,
    "xorig": 27,
    "yorig": 14
}