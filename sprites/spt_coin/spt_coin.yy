{
    "id": "08c73c1f-12e6-4b30-9a24-9f2c76a6c021",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spt_coin",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 10,
    "bbox_left": 1,
    "bbox_right": 8,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7fcdb0b9-1807-476f-9180-0983f95cab45",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "08c73c1f-12e6-4b30-9a24-9f2c76a6c021",
            "compositeImage": {
                "id": "500836fc-922e-48d9-adf9-4ee049449e8a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7fcdb0b9-1807-476f-9180-0983f95cab45",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5314c201-651a-497f-a794-e519929ea314",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7fcdb0b9-1807-476f-9180-0983f95cab45",
                    "LayerId": "5db477e7-7a30-48b7-a45a-dd598f8d7bf2"
                }
            ]
        },
        {
            "id": "274f70e7-2c19-4300-8a46-1157a8912483",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "08c73c1f-12e6-4b30-9a24-9f2c76a6c021",
            "compositeImage": {
                "id": "15ab1b3c-85fd-41d0-bde3-7b023f65659e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "274f70e7-2c19-4300-8a46-1157a8912483",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ed7616e5-3206-4a99-8f15-d6cbe5169393",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "274f70e7-2c19-4300-8a46-1157a8912483",
                    "LayerId": "5db477e7-7a30-48b7-a45a-dd598f8d7bf2"
                }
            ]
        },
        {
            "id": "365ffd23-a15f-47de-97b3-1c8d17904b98",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "08c73c1f-12e6-4b30-9a24-9f2c76a6c021",
            "compositeImage": {
                "id": "e83e3077-018b-463a-8d6e-ffe44ef3cdd7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "365ffd23-a15f-47de-97b3-1c8d17904b98",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3304f9a8-5147-4805-831d-519a39e89532",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "365ffd23-a15f-47de-97b3-1c8d17904b98",
                    "LayerId": "5db477e7-7a30-48b7-a45a-dd598f8d7bf2"
                }
            ]
        },
        {
            "id": "f6143378-829f-4880-b7f4-bd74866f761b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "08c73c1f-12e6-4b30-9a24-9f2c76a6c021",
            "compositeImage": {
                "id": "ac90f2e6-7151-412e-b668-7fef75c63373",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f6143378-829f-4880-b7f4-bd74866f761b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ef2dbe09-56ed-4f0b-b7f3-77fdcbcafbe2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f6143378-829f-4880-b7f4-bd74866f761b",
                    "LayerId": "5db477e7-7a30-48b7-a45a-dd598f8d7bf2"
                }
            ]
        },
        {
            "id": "eabf70e8-9ceb-4ecb-89c3-a8c47514f5cc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "08c73c1f-12e6-4b30-9a24-9f2c76a6c021",
            "compositeImage": {
                "id": "3e217b68-3a56-4c43-aa97-2739cd0fc25f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eabf70e8-9ceb-4ecb-89c3-a8c47514f5cc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "29b29b4d-493d-4bd9-bc54-ee4e66e58d3c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eabf70e8-9ceb-4ecb-89c3-a8c47514f5cc",
                    "LayerId": "5db477e7-7a30-48b7-a45a-dd598f8d7bf2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 11,
    "layers": [
        {
            "id": "5db477e7-7a30-48b7-a45a-dd598f8d7bf2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "08c73c1f-12e6-4b30-9a24-9f2c76a6c021",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 3,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 9,
    "xorig": 4,
    "yorig": 5
}