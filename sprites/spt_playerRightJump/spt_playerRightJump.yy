{
    "id": "b93176ea-cbf3-4074-b0dc-9fd590b7ed8f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spt_playerRightJump",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 24,
    "bbox_left": 0,
    "bbox_right": 14,
    "bbox_top": 4,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5b7bf81a-438d-4aab-9bde-70b21cc67a11",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b93176ea-cbf3-4074-b0dc-9fd590b7ed8f",
            "compositeImage": {
                "id": "d2b58211-1800-472b-9046-0151a2e35272",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5b7bf81a-438d-4aab-9bde-70b21cc67a11",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1425254e-f7bd-40a1-8855-32ceb9b16385",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5b7bf81a-438d-4aab-9bde-70b21cc67a11",
                    "LayerId": "63f050e9-7bcb-45b1-99a5-ab9ea592cc3a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 25,
    "layers": [
        {
            "id": "63f050e9-7bcb-45b1-99a5-ab9ea592cc3a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b93176ea-cbf3-4074-b0dc-9fd590b7ed8f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 25,
    "xorig": 12,
    "yorig": 12
}