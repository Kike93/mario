{
    "id": "4e9ec515-65fb-4250-9e40-24e56a40d72c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spt_playerStayRight",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 24,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 3,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5c5212e9-da62-4736-9a8b-f38dabc665a6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e9ec515-65fb-4250-9e40-24e56a40d72c",
            "compositeImage": {
                "id": "f77fd9a7-3e18-433c-9c98-be92837f9133",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5c5212e9-da62-4736-9a8b-f38dabc665a6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "17bee67e-08d0-43c0-b1ba-c673a6792cd5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5c5212e9-da62-4736-9a8b-f38dabc665a6",
                    "LayerId": "7f725975-05d2-4572-84fc-783e2e6a3bae"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 25,
    "layers": [
        {
            "id": "7f725975-05d2-4572-84fc-783e2e6a3bae",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4e9ec515-65fb-4250-9e40-24e56a40d72c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 6,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 25,
    "xorig": 12,
    "yorig": 12
}