{
    "id": "0cb6b4b3-2d7c-4f5c-8cad-c727671891d0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spt_playerLeftJump",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 24,
    "bbox_left": 0,
    "bbox_right": 14,
    "bbox_top": 4,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "aca072f3-1dd7-4bae-93e9-575d7909377f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0cb6b4b3-2d7c-4f5c-8cad-c727671891d0",
            "compositeImage": {
                "id": "d6a1326d-f1d3-4363-8a35-2dbfb4c23ac4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aca072f3-1dd7-4bae-93e9-575d7909377f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "657db9e8-a448-46a6-b761-4cdc002ccf47",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aca072f3-1dd7-4bae-93e9-575d7909377f",
                    "LayerId": "8a66ec16-cb86-4d13-a553-7ee0f4cba370"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 25,
    "layers": [
        {
            "id": "8a66ec16-cb86-4d13-a553-7ee0f4cba370",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0cb6b4b3-2d7c-4f5c-8cad-c727671891d0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 25,
    "xorig": 12,
    "yorig": 12
}