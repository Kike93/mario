{
    "id": "85bfd43b-81fe-4e7a-acee-766c04bab425",
    "modelName": "GMFont",
    "mvc": "1.0",
    "name": "fnt_mario",
    "AntiAlias": 1,
    "TTFName": "",
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Press Start 2P",
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "7bc178f4-bea4-4ba7-9740-c82bf638e915",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 8,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "ce6aaba6-b183-4365-a72b-f3b8767e3ca0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 8,
                "offset": 2,
                "shift": 8,
                "w": 3,
                "x": 48,
                "y": 62
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "902d7177-a9e7-44d4-9635-d37c47fccbfc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 8,
                "offset": 1,
                "shift": 8,
                "w": 5,
                "x": 82,
                "y": 52
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "f174feb1-2af5-4732-8c14-7b4fe85c9235",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 8,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 20,
                "y": 42
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "d1493e54-edee-408f-bd06-3d337c0c4fea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 8,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 56,
                "y": 42
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "2a878f4b-42a5-494c-8c84-3e7fb29f7a5c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 8,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 2,
                "y": 42
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "17a76f81-d84f-4047-8b13-11ec4a4cc492",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 8,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 101,
                "y": 32
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "963428cb-e4e2-4e5d-b20a-f09a0c139784",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 8,
                "offset": 2,
                "shift": 8,
                "w": 2,
                "x": 53,
                "y": 62
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "30582321-9bf0-4062-a9bf-d7827cde186b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 8,
                "offset": 2,
                "shift": 8,
                "w": 4,
                "x": 2,
                "y": 62
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "ca34648f-7e66-46e7-bec1-0e063b92414c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 8,
                "offset": 1,
                "shift": 8,
                "w": 4,
                "x": 32,
                "y": 62
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "9cf3f125-1705-491c-a18d-44b8dd6da942",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 8,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 65,
                "y": 32
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "02508c35-6834-4492-a198-f24bf2458a80",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 8,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 101,
                "y": 42
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "1cc55c30-fbb0-4d8b-b691-ef308c9ef19d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 8,
                "offset": 1,
                "shift": 8,
                "w": 3,
                "x": 43,
                "y": 62
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "1e8fe374-3f85-4f73-8765-cac061e88864",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 8,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 10,
                "y": 52
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "d97fe7c1-0b1c-4172-96ac-17eed4c12d92",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 8,
                "offset": 2,
                "shift": 8,
                "w": 2,
                "x": 69,
                "y": 62
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "d8191b54-b8b2-4a57-92eb-9d6c8ce2ce26",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 8,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 92,
                "y": 42
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "644df466-a906-4e4b-87f4-35f2b936f4ea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 8,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 92,
                "y": 32
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "5406c1f0-e6bd-4756-b853-50c9e805f76c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 8,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 34,
                "y": 52
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "eb57cd3d-5feb-4b74-bef1-da5f53e7a4b7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 8,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 20,
                "y": 32
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "5b507d1f-abb1-4dda-a324-059fcfc29a60",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 8,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 29,
                "y": 32
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "8742f0a6-5900-48a1-ba56-b2bd0109064a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 8,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 38,
                "y": 32
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "e42f1868-dfa5-4562-9938-c974a364ab97",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 8,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 47,
                "y": 32
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "ac72c243-1a36-4524-ab23-d7c2cb4c2adc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 8,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 56,
                "y": 32
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "d95f2520-090c-468f-9f51-eb913678dd41",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 8,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 74,
                "y": 32
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "ad9ca469-cb94-480e-84fa-a10d9e8e12c6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 8,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 83,
                "y": 32
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "1fcd2f9f-e44e-4a78-b27b-8ff11d81b1e2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 8,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 110,
                "y": 32
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "a1fd4a51-2db0-467e-ac5c-4557fa305ac7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 8,
                "offset": 2,
                "shift": 8,
                "w": 2,
                "x": 57,
                "y": 62
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "aa401796-cd8a-45e9-abd5-86f6ea169068",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 8,
                "offset": 1,
                "shift": 8,
                "w": 3,
                "x": 38,
                "y": 62
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "9c742834-2fb2-42f4-b541-cfe097618196",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 8,
                "offset": 1,
                "shift": 8,
                "w": 5,
                "x": 96,
                "y": 52
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "a5634699-ed63-4185-8114-ac02395452bd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 8,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 74,
                "y": 42
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "040ab532-3d4d-457f-96a1-ffe61cf8b732",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 8,
                "offset": 1,
                "shift": 8,
                "w": 5,
                "x": 89,
                "y": 52
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "90f9b4ed-7ea4-45da-a0b0-64f766421df1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 8,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 65,
                "y": 42
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "fd8c6bc2-4008-4a7a-acf8-a8fff26cc2cd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 8,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 83,
                "y": 42
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "95b616a0-3db1-408d-8d41-778216284bfe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 8,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 47,
                "y": 42
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "d82a8457-1758-4da7-b192-b780beface17",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 8,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 38,
                "y": 42
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "a4857bf2-d398-4ff5-a8d2-07e323f4667e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 8,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 29,
                "y": 42
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "5dbd77e3-3372-4c0a-85d7-09f3849e7711",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 8,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 11,
                "y": 42
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "4e31ed2c-c313-4fdf-920a-32a4f6bdfbbd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 8,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 2,
                "y": 32
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "e4b4b38c-b3e9-4ff0-9225-69344035770e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 8,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 11,
                "y": 32
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "ece4a7fe-003e-4ce7-a0b0-9d7a01b472eb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 8,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 101,
                "y": 22
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "5449955a-3df9-480a-ba0a-006436120265",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 8,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 11,
                "y": 22
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "948f00c7-dd0b-47a1-9fd4-7c2e6e3d1c28",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 8,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 50,
                "y": 52
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "e2ecffed-88f8-450d-8719-e44ade225c67",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 8,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 20,
                "y": 12
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "13eee1a7-1000-4be0-a11e-575050c8ed72",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 8,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 11,
                "y": 12
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "603dc43a-b356-4667-a4af-2c84e9cb61bd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 8,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 58,
                "y": 52
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "45fc67e8-49a5-4215-a3c4-d223796ce896",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 8,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 2,
                "y": 12
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "db1cffd4-71c8-4767-af29-e7314216f98a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 8,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 111,
                "y": 2
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "b7e82a47-9f92-4e92-9059-11fa4f6e4644",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 8,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 110,
                "y": 22
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "4585a3a4-f90d-434d-9e5e-5e3026e46aed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 8,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 93,
                "y": 2
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "f9ea0ad7-379e-4c62-b15a-266b06a836ae",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 8,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 84,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "f552a0aa-3ae1-43bf-95fd-09e4d2b97817",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 8,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 75,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "dc06232b-dd09-42fe-b781-6b7c913b9236",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 8,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 66,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "c9da34fe-37a6-4258-aac6-26df0bf80664",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 8,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 42,
                "y": 52
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "cebdf256-731b-47bd-a379-fdd786e9aeee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 8,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 57,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "362df20a-5ac5-4813-a4e4-df5eefca32e8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 8,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 48,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "b2908bdf-15fa-425f-82ae-afec7f14b7d5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 8,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 39,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "eb52032b-3310-4f82-850a-3134269397b1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 8,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 30,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "3d7f6cce-a4f0-43c3-9778-0f2058bd4051",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 8,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 74,
                "y": 52
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "2cf44041-abd4-4c6b-9ad4-bb028fa2947a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 8,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 21,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "2ec29bfd-b15d-47b6-b0d8-daa9585beae9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 8,
                "offset": 2,
                "shift": 8,
                "w": 4,
                "x": 8,
                "y": 62
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "760ba142-2275-4959-b98b-3b6e5fa6d213",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 8,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 12,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "bcb83af8-7263-4daa-bcfb-ea9b397e23c9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 8,
                "offset": 1,
                "shift": 8,
                "w": 4,
                "x": 14,
                "y": 62
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "884030c0-9c20-4462-ab62-9ed0670b66ab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 8,
                "offset": 1,
                "shift": 8,
                "w": 5,
                "x": 117,
                "y": 52
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "260851ce-215a-4ec6-b1fa-2024ca9403fb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 8,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 29,
                "y": 12
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "a1dbfd0f-a158-4827-81ba-4f33a775ba09",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 8,
                "offset": 3,
                "shift": 8,
                "w": 2,
                "x": 61,
                "y": 62
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "da8562bb-3253-48b7-8601-031afce05f4a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 8,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 38,
                "y": 12
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "5d92cf85-9159-4f20-a48a-49b9dabf929d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 8,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 47,
                "y": 12
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "61624d02-2023-4b5a-a186-9114e672a266",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 8,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 56,
                "y": 12
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "295ee900-8ff9-4225-bcdd-230f9fd30ac0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 8,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 83,
                "y": 22
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "631aa370-384c-4eee-a7dc-56a784792743",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 8,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 74,
                "y": 22
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "3328ae45-ae56-4261-b923-71eea9b55f3a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 8,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 66,
                "y": 52
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "c3c7e2b5-75b6-4ec7-b5db-dc12305fc6f1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 8,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 65,
                "y": 22
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "cefab574-778a-41ee-8d58-855367acb8d3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 8,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 56,
                "y": 22
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "08c6e970-4883-43ae-b1e3-e8eeaa8a687f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 8,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 2,
                "y": 52
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "e85436e5-e765-40d3-afb7-d5757577d31e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 8,
                "offset": 1,
                "shift": 8,
                "w": 5,
                "x": 110,
                "y": 52
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "3215c5d7-5420-486d-aef6-14d6ca715d7e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 8,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 47,
                "y": 22
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "32358b3f-6d07-4e8e-9205-3ecb128f9de2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 8,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 117,
                "y": 42
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "96603b82-72ab-41bc-bc36-2b930b20c406",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 8,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 38,
                "y": 22
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "b66e7bab-d5e5-4ed3-9817-7725dbdc23a6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 8,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 29,
                "y": 22
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "a55dfab6-41f5-4561-940d-ed806f654e0c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 8,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 92,
                "y": 22
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "854d1f51-06f4-4503-aced-be7a180c3a42",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 8,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 20,
                "y": 22
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "6324ee0f-3ff7-4a39-837a-addf2f3305b2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 8,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 2,
                "y": 22
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "ca604822-21f9-4dc6-a1f4-48dda470d7c8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 8,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 26,
                "y": 52
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "d64f6478-5a59-46a5-9059-16297cfa6a34",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 8,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 110,
                "y": 12
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "bd25b530-7be3-418f-8b76-db25a7c8d1c2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 8,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 109,
                "y": 42
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "27bb9776-da60-4a77-9631-efc537c328a6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 8,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 101,
                "y": 12
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "5ab6c3d4-1baa-4c4c-b5fe-1a77d3ec63b8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 8,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 18,
                "y": 52
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "0058b254-ee05-4d0d-b49b-d8f138f770dc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 8,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 92,
                "y": 12
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "90cebb10-d3e5-4725-bc5c-ee2d0e7c031f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 8,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 83,
                "y": 12
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "e6c2981b-6071-4433-b8db-34dd16d0e63b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 8,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 74,
                "y": 12
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "11e52338-f728-4edf-88e7-31b1fe89b823",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 8,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 65,
                "y": 12
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "bada249d-68f0-49c2-b661-ae81347684c1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 8,
                "offset": 2,
                "shift": 8,
                "w": 4,
                "x": 20,
                "y": 62
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "976a3cea-cf76-4544-8a4d-b043a184a3e1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 8,
                "offset": 3,
                "shift": 8,
                "w": 2,
                "x": 65,
                "y": 62
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "7d1b2440-9484-4323-9835-5332ac8ff1e7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 8,
                "offset": 1,
                "shift": 8,
                "w": 4,
                "x": 26,
                "y": 62
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "3dc79673-7ee9-4395-b11b-4a1ecd32e08c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 8,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 102,
                "y": 2
            }
        },
        {
            "Key": 127,
            "Value": {
                "id": "265b84ab-fd18-44b4-81a8-c2823d137503",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 127,
                "h": 8,
                "offset": 1,
                "shift": 8,
                "w": 5,
                "x": 103,
                "y": 52
            }
        }
    ],
    "image": null,
    "includeTTF": false,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 6,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}