grav = 0.2;
hsp = 0;
vsp = 0;

jumpspeed = 4.5;
movespeed = 1.5;

//Constants
grounded = false;
jumping = false;

//Horizontal jump constants
hsp_jump_constant_small = 1;
hsp_jump_constant_big = 3;
hsp_jump_applied = 0;

//Horizontal key pressed count
hkp_count = 0;
hkp_count_small = 1;
hkp_count_big = 5;

//Init variables
key_left = 0;
key_right = 0;
key_jump = false;

image_speed = 5;

