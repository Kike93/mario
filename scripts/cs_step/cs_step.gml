move = key_left + key_right;
hsp = move*movespeed;

if(vsp<10)vsp+=grav;

if(place_meeting(x,y+1,obj_wall))
{
    //Check if recently grounded
    if(!grounded && !key_jump){
        hkp_count = 0; //Init horizontal count
        jumping = false;
    }else if(grounded && key_jump){ //recently jumping
		audio_play_sound(snd_jump, 1, false);
        jumping = true;
    }

    //Check if player grounded
    grounded = !key_jump;
    
    vsp = key_jump * -jumpspeed;
}

if(place_meeting(x,y+1,obj_base))
{
    //Check if recently grounded
    if(!grounded && !key_jump){
        hkp_count = 0; //Init horizontal count
        jumping = false;
    }else if(grounded && key_jump){ //recently jumping
		audio_play_sound(snd_jump, 1, false);
        jumping = true;
    }

    //Check if player grounded
    grounded = !key_jump;
    
    vsp = key_jump * -jumpspeed;
}

if(place_meeting(x,y+1,obj_pow))
{
    //Check if recently grounded
    if(!grounded && !key_jump){
        hkp_count = 0; //Init horizontal count
        jumping = false;
    }else if(grounded && key_jump){ //recently jumping
		audio_play_sound(snd_jump, 1, false);
        jumping = true;
    }

    //Check if player grounded
    grounded = !key_jump;
    
    vsp = key_jump * -jumpspeed;
}



//Init hsp_jump_applied
if(grounded){
    hsp_jump_applied = 0;
}

//Check horizontal counts
if(move!=0 && grounded){
 hkp_count++;
}else if(move==0 && grounded){
 hkp_count=0;
}

//Check jumping
if(jumping){
    
    //check if previously we have jump
	if(jumping){
		hsp_jump_applied = sign(move);       
	}

    //don't jump horizontal
    if(hkp_count < hkp_count_small ){
        hsp = 0;
    }else if(hkp_count > hkp_count_small && hkp_count < hkp_count_big){ //small jump
        hsp = hsp_jump_applied * hsp_jump_constant_small;
    }else{ // big jump
        hsp = hsp_jump_applied *hsp_jump_constant_big
    }
	
}

//horizontal collision
if(place_meeting(x+hsp,y,obj_wall))
{
    while(!place_meeting(x+sign(hsp),y,obj_wall))
    {
        x+= sign(hsp);
    }
    hsp = 0;
}


//vertical collision
if(place_meeting(x,y+vsp,obj_wall))
{
    while(!place_meeting(x, sign(vsp) + y,obj_wall))
    {
        y+= sign(vsp);
    }
    vsp = 0;
}

//horizontal collision
if(place_meeting(x+hsp,y,obj_base))
{
    while(!place_meeting(x+sign(hsp),y,obj_base))
    {
        x+= sign(hsp);
    }
    hsp = 0;
}


//vertical collision
if(place_meeting(x,y+vsp,obj_base))
{
    while(!place_meeting(x, sign(vsp) + y,obj_base))
    {
        y+= sign(vsp);
    }
    vsp = 0;
}

//horizontal collision
if(place_meeting(x+hsp,y,obj_pow))
{
    while(!place_meeting(x+sign(hsp),y,obj_pow))
    {
        x+= sign(hsp);
    }
    hsp = 0;
}


//vertical collision
if(place_meeting(x,y+vsp,obj_pow))
{
    while(!place_meeting(x, sign(vsp) + y,obj_pow))
    {
        y+= sign(vsp);
    }
    vsp = 0;
}


x+= hsp;
y+= vsp;




