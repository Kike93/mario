{
    "id": "d8ae1cfe-8098-4f8f-9e4d-c0e558db52cc",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_enemy2",
    "eventList": [
        {
            "id": "e6cd3cd9-2570-4890-8b2c-9e40abb38832",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "d8ae1cfe-8098-4f8f-9e4d-c0e558db52cc"
        },
        {
            "id": "f00cc3e0-447b-4d01-afe5-dcf7360a0529",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "d8ae1cfe-8098-4f8f-9e4d-c0e558db52cc"
        },
        {
            "id": "db5e302a-3b4a-40a3-9b03-b1613bea958c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 7,
            "m_owner": "d8ae1cfe-8098-4f8f-9e4d-c0e558db52cc"
        },
        {
            "id": "d9fb25ee-e4db-4446-a7a3-a91d4f1b7dba",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "d8ae1cfe-8098-4f8f-9e4d-c0e558db52cc",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "d8ae1cfe-8098-4f8f-9e4d-c0e558db52cc"
        },
        {
            "id": "92d3a804-19a0-4870-9cc7-d58b26c335cf",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "6f00bb1f-d25c-434c-b251-9806181eae3e",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "d8ae1cfe-8098-4f8f-9e4d-c0e558db52cc"
        },
        {
            "id": "dc6182fa-dd2e-4b5b-8965-b83f71653fb8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "7d65252d-d5f4-49c4-b486-d26af237c4f9",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "d8ae1cfe-8098-4f8f-9e4d-c0e558db52cc"
        }
    ],
    "maskSpriteId": "acececf4-d599-4fad-bdc9-01bdc54332a4",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "acececf4-d599-4fad-bdc9-01bdc54332a4",
    "visible": true
}