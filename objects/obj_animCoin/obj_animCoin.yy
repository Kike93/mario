{
    "id": "e40116a3-dcba-495c-9083-4ea3bf005b7a",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_animCoin",
    "eventList": [
        {
            "id": "748e8336-45d8-47c4-8389-6dd170189160",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "e40116a3-dcba-495c-9083-4ea3bf005b7a"
        }
    ],
    "maskSpriteId": "2f43332b-126d-4247-affc-a984ccfa9b2d",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "2f43332b-126d-4247-affc-a984ccfa9b2d",
    "visible": true
}