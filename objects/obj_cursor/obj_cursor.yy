{
    "id": "5fe54c11-0dcc-4598-8677-255e5a705211",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_cursor",
    "eventList": [
        {
            "id": "3d0d638f-7873-4dd3-82c6-078dc1cac1e4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "5fe54c11-0dcc-4598-8677-255e5a705211"
        },
        {
            "id": "a3308106-3a9f-4678-bb75-dba5bab6d9b0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "5fe54c11-0dcc-4598-8677-255e5a705211"
        },
        {
            "id": "46f48047-2c21-4772-8628-1d648e75a952",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 32,
            "eventtype": 9,
            "m_owner": "5fe54c11-0dcc-4598-8677-255e5a705211"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "38dbacd1-e67f-430e-810e-2e6a15e944e6",
    "visible": true
}