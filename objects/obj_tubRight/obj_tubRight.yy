{
    "id": "6c4e554e-9ffd-4642-bd1b-2b73a7a4ceac",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_tubRight",
    "eventList": [
        {
            "id": "08e32d3b-a6df-4c2b-9fb0-447d85466cff",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "6c4e554e-9ffd-4642-bd1b-2b73a7a4ceac"
        },
        {
            "id": "cb8a77dc-4008-47a4-b752-1511d9b44c36",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "6c4e554e-9ffd-4642-bd1b-2b73a7a4ceac"
        },
        {
            "id": "7e38a164-33a1-4b85-8103-edfd8a90753a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "af8551b2-efed-4aaf-a7a9-3aa25494d64d",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "6c4e554e-9ffd-4642-bd1b-2b73a7a4ceac"
        },
        {
            "id": "0664e1b5-fed2-46f0-bb8d-f6f60ae7f05a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "6f00bb1f-d25c-434c-b251-9806181eae3e",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "6c4e554e-9ffd-4642-bd1b-2b73a7a4ceac"
        },
        {
            "id": "95cbe16c-132b-4ba1-8b0a-066d381beb83",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "d8ae1cfe-8098-4f8f-9e4d-c0e558db52cc",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "6c4e554e-9ffd-4642-bd1b-2b73a7a4ceac"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "b8e66268-32a5-45de-8c71-7b95ac0415f5",
    "visible": true
}