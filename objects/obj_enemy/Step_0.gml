if(!enemyDead){
	if(!enemyDown){
		key_right = 1;
		if(movespeed == 1){
			image_speed = 1;
			sprite_index = spt_enemyLeft;
		} 

		if(movespeed == -1){
			image_speed = 1;
			sprite_index = spt_enemyRight;
		}
			
	} else {
		key_right = 0;
		sprite_index = spt_enemyDown;
		
		timeTurtleUp -= 1;
		if(timeTurtleUp == 0){
			enemyDown = false;
			timeTurtleUp = room_speed * 5;
		}
	}
	
	if(pushEnemy){
		y -= 14;
		pushEnemy = false;
	}
	
	if(timerColision > 0){
		timerColision -= 1;
	}
	
	script_execute(cs_step,0,0,0,0,0);
} else {
	y += 5;
	if(y > room_height){
		if(sonidoAgua){
			audio_play_sound(snd_caidaAgua, 1, false);
			sonidoAgua = false;
		}
		
		instance_create_depth(x,y,depth-10, obj_water);
		timeCoin -= 1;
	}
	
	if(timeCoin == 0){
		if(movespeed == 1){
			coin = instance_create_depth(obj_tuberiaRigth.x,obj_tuberiaRigth.y,depth-10, obj_coin);
			coin.movespeed = 1;
		}
		if(movespeed == -1){
			coin = instance_create_depth(obj_tuberiaLeft.x,obj_tuberiaLeft.y,depth-10, obj_coin);
			coin.movespeed = -1;
		}
		instance_destroy();
		if(!instance_exists(obj_enemy)){
			if(obj_changes.numeroRoom == 1){
				obj_changes.numeroRoom = 2;
				obj_changes.puntosGuardados = obj_game.puntos;
				room_goto(room1);
			}else if(obj_changes.numeroRoom == 2){
				obj_changes.numeroRoom = 1;
				obj_changes.puntosGuardados = obj_game.puntos;
				room_goto(room0);
			}
		}

	}
}