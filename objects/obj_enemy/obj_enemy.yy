{
    "id": "af8551b2-efed-4aaf-a7a9-3aa25494d64d",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_enemy",
    "eventList": [
        {
            "id": "96508762-3cc9-4a16-9093-bfb68995141e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "af8551b2-efed-4aaf-a7a9-3aa25494d64d"
        },
        {
            "id": "f5247b62-0e2b-434b-865c-9722c7af341c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "af8551b2-efed-4aaf-a7a9-3aa25494d64d"
        },
        {
            "id": "691c80ef-35c5-4ed2-a886-7b6ed8914375",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 7,
            "m_owner": "af8551b2-efed-4aaf-a7a9-3aa25494d64d"
        },
        {
            "id": "1b4fa7f6-bdc2-433a-ac32-bd0eb14e13a9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "af8551b2-efed-4aaf-a7a9-3aa25494d64d",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "af8551b2-efed-4aaf-a7a9-3aa25494d64d"
        },
        {
            "id": "b5e41a19-7e59-45ef-bd7d-a3739dc5cb13",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "6f00bb1f-d25c-434c-b251-9806181eae3e",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "af8551b2-efed-4aaf-a7a9-3aa25494d64d"
        },
        {
            "id": "81490572-bb8f-4d0f-986c-f8b031ad5a8d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "7d65252d-d5f4-49c4-b486-d26af237c4f9",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "af8551b2-efed-4aaf-a7a9-3aa25494d64d"
        }
    ],
    "maskSpriteId": "458c4edc-feac-4132-90fb-77b5258a3ebc",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": [
        {
            "id": "5a4c2f00-fc2d-4ef8-843b-01119dfe23cf",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 0
        },
        {
            "id": "abe08eac-71f6-48b9-8688-a63e92760fcb",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 12,
            "y": 0
        },
        {
            "id": "51aa68c7-0968-4b7c-a93b-5190fc64c272",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 12,
            "y": 16
        },
        {
            "id": "85e9aa10-2f5d-48cd-9a33-32174b8da075",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 16
        }
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "3863b400-a9b3-4f67-9f47-4d747fa92619",
    "visible": true
}