{
    "id": "7d65252d-d5f4-49c4-b486-d26af237c4f9",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_wall",
    "eventList": [
        {
            "id": "863bee61-54b8-4ad2-ad6d-85d019131961",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "7d65252d-d5f4-49c4-b486-d26af237c4f9"
        },
        {
            "id": "d3e7dab3-da2d-453f-93b0-2b6c09c438f7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "7d65252d-d5f4-49c4-b486-d26af237c4f9"
        }
    ],
    "maskSpriteId": "f2a21416-4c37-4b72-9594-697d4b992fe0",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "f2a21416-4c37-4b72-9594-697d4b992fe0",
    "visible": true
}