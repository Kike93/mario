{
    "id": "c4dbcbb7-0174-4beb-b1a3-2d682755a473",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_tuberiaLeft",
    "eventList": [
        {
            "id": "2cd8bf9a-5057-453f-9e2b-01a97a80f4ba",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "c4dbcbb7-0174-4beb-b1a3-2d682755a473"
        },
        {
            "id": "43e4f709-24a0-47cc-89c9-f6389668ef8c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "c4dbcbb7-0174-4beb-b1a3-2d682755a473"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "63f25a1e-5035-4e83-9bbc-47fad97924e4",
    "visible": true
}