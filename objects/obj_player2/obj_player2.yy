{
    "id": "68d552fe-e86c-4963-aa17-703ab3231b73",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_player2",
    "eventList": [
        {
            "id": "bcbc60a5-fe51-43fa-b9de-b698c99fcb45",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "68d552fe-e86c-4963-aa17-703ab3231b73"
        },
        {
            "id": "84a655c8-8b12-4fe3-90bf-c2dcad409f4f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "68d552fe-e86c-4963-aa17-703ab3231b73"
        },
        {
            "id": "6e65c693-f287-4549-8a18-85cb59e8f754",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 7,
            "m_owner": "68d552fe-e86c-4963-aa17-703ab3231b73"
        },
        {
            "id": "61bd97ca-980e-47d5-9f72-0b0b86d04ed1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "af8551b2-efed-4aaf-a7a9-3aa25494d64d",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "68d552fe-e86c-4963-aa17-703ab3231b73"
        },
        {
            "id": "5ecb3db3-8490-453f-acef-210ee86840b4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "6f00bb1f-d25c-434c-b251-9806181eae3e",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "68d552fe-e86c-4963-aa17-703ab3231b73"
        },
        {
            "id": "b40bc90c-91ec-4d19-960f-e9621163361e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "d8ae1cfe-8098-4f8f-9e4d-c0e558db52cc",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "68d552fe-e86c-4963-aa17-703ab3231b73"
        }
    ],
    "maskSpriteId": "4e9ec515-65fb-4250-9e40-24e56a40d72c",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "4e9ec515-65fb-4250-9e40-24e56a40d72c",
    "visible": true
}