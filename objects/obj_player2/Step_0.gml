key_right = keyboard_check(vk_right);
key_left = -keyboard_check(vk_left);
key_jump = keyboard_check_pressed(vk_space);

if(dead){
	
	sprite_index = spt_playerDead;
	image_speed = 1;
	y = y - 1;
	timer -= 1;
	
	if(sonidoDead && obj_changes.vida <= 0){
		audio_play_sound(snd_muerte, 1, false);
		sonidoDead = false;
	}
	
	
	if(timer == 0) {
		
		if(obj_changes.vida > 0){
			if(obj_changes.vida == 3){
				instance_destroy(obj_life2);
			}
			
			if(obj_changes.vida == 2){
				instance_destroy(obj_life1);
			}
			
			if(obj_changes.vida == 1){
				instance_destroy(obj_life);
			}
			
			obj_changes.vida -= 1;
			obj_game.volverVida = true;
		} else {
			
			highscore_add("Score", obj_game.puntos);
			obj_changes.numeroRoom = 0;
			obj_changes.puntosGuardados = 0;
			obj_changes.vida = 3;
			obj_changes.numeroSala = 0;
			room_goto(room_start);
		}
		instance_destroy();
	}
	
} else {

	script_execute(cs_step,0,0,0,0,0);
	
	if(keyboard_check(vk_right) && !jumping){
		image_speed = 1;
		sprite_index = spt_playerLeft;
	} 
		
	if(keyboard_check(vk_left) && !jumping){
		image_speed = 1;
		sprite_index = spt_playerRight;
	}

	if(!keyboard_check(vk_left) && !keyboard_check(vk_right)){
		image_speed = 0;
		sprite_index = spt_playerStayRight;
	}


	if(jumping && keyboard_check(vk_right)){
		image_speed = 0;
		sprite_index = spt_playerLeftJump;
	}

	if(keyboard_check(vk_left) && jumping){
		image_speed = 0;
		sprite_index = spt_playerRightJump;
	}
}