{
    "id": "6b3f9dfe-2933-49f1-83e5-d27bdb661bdd",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_tubLeft",
    "eventList": [
        {
            "id": "9c190036-175b-4308-ac90-5bae91acf483",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "af8551b2-efed-4aaf-a7a9-3aa25494d64d",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "6b3f9dfe-2933-49f1-83e5-d27bdb661bdd"
        },
        {
            "id": "325ddeb7-be76-45f7-9e42-5e69ba88a9e7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "6b3f9dfe-2933-49f1-83e5-d27bdb661bdd"
        },
        {
            "id": "15687c84-9946-4455-bf55-a7d95543a82f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "6b3f9dfe-2933-49f1-83e5-d27bdb661bdd"
        },
        {
            "id": "e1921ba1-242d-42be-a478-916646fb02e7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "6f00bb1f-d25c-434c-b251-9806181eae3e",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "6b3f9dfe-2933-49f1-83e5-d27bdb661bdd"
        },
        {
            "id": "74cdb23e-3af7-41e2-9c2d-eabb65933248",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "d8ae1cfe-8098-4f8f-9e4d-c0e558db52cc",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "6b3f9dfe-2933-49f1-83e5-d27bdb661bdd"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "589026d4-cf6b-4f70-97f2-f263e5d3924d",
    "visible": true
}