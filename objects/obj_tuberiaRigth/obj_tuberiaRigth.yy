{
    "id": "785fb165-a609-4c8c-a610-47f67768dc74",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_tuberiaRigth",
    "eventList": [
        {
            "id": "9a10d121-ffc4-447e-a9f6-e00fb3bfe80a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "785fb165-a609-4c8c-a610-47f67768dc74"
        },
        {
            "id": "e5baf25d-0e5b-4343-b337-05c8330a9f08",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "785fb165-a609-4c8c-a610-47f67768dc74"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "1a808559-d70c-4b95-92b0-28d02dce8e50",
    "visible": true
}