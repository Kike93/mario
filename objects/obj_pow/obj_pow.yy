{
    "id": "6db0284f-485d-4c9f-9641-4cf742ddfb61",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_pow",
    "eventList": [
        {
            "id": "e43eaf74-9bfc-4b2d-bad5-27b4a0f4abaf",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "6db0284f-485d-4c9f-9641-4cf742ddfb61"
        },
        {
            "id": "77b05375-abab-4ab7-9e25-20acce3c79fa",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "6db0284f-485d-4c9f-9641-4cf742ddfb61"
        }
    ],
    "maskSpriteId": "16c4fd08-801a-4318-8007-2db642af41f3",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "16c4fd08-801a-4318-8007-2db642af41f3",
    "visible": true
}