{
    "id": "39e107eb-c8ae-4183-984f-728c0d914227",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_water",
    "eventList": [
        {
            "id": "b2cdb33b-cc3a-44ee-afad-83d7e37ecb04",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "39e107eb-c8ae-4183-984f-728c0d914227"
        }
    ],
    "maskSpriteId": "508ca79b-fa34-4501-9255-dddf0794a86f",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "508ca79b-fa34-4501-9255-dddf0794a86f",
    "visible": true
}