{
    "id": "6f00bb1f-d25c-434c-b251-9806181eae3e",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_coin",
    "eventList": [
        {
            "id": "c7b23e52-9274-4263-b869-85f6921cac0f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "6f00bb1f-d25c-434c-b251-9806181eae3e"
        },
        {
            "id": "9de885ee-498a-4e2c-84c8-916f2735c38c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "6f00bb1f-d25c-434c-b251-9806181eae3e"
        },
        {
            "id": "7fc4a151-c8ae-4cfc-a174-13b5d15d8e90",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 7,
            "m_owner": "6f00bb1f-d25c-434c-b251-9806181eae3e"
        },
        {
            "id": "4184aa9e-4ab5-4c49-8cb8-d4c66a291454",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "6f00bb1f-d25c-434c-b251-9806181eae3e",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "6f00bb1f-d25c-434c-b251-9806181eae3e"
        }
    ],
    "maskSpriteId": "08c73c1f-12e6-4b30-9a24-9f2c76a6c021",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "08c73c1f-12e6-4b30-9a24-9f2c76a6c021",
    "visible": true
}